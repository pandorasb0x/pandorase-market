package com.pandorasbox.items.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pandorasbox.items.models.Item;
import com.pandorasbox.items.models.service.*;

@RestController
public class ItemController {
	
	@Autowired
	private ItemService itemService;
	
	@GetMapping("/items")
	public List<Item> getItems(){
		return itemService.findall();
	}
	
	@GetMapping("/items/{id}")
	public Item getDetailedItem(@PathVariable Long id,@RequestParam Integer quantity ){
		return itemService.findById(id,quantity);
	}
	

}
