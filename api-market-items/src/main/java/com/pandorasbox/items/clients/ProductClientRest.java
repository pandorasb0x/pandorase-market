package com.pandorasbox.items.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.pandorasbox.items.models.Product;

@FeignClient(name = "service-products")
public interface ProductClientRest {
	
	@GetMapping("/products")
	public List<Product> products();

	@GetMapping("/products/{id}")
	public Product getDetailedProd(@PathVariable Long id);
}
