package com.pandorasbox.items.models.service;

import java.util.List;

import com.pandorasbox.items.models.Item;

public interface ItemService {
	
	public List<Item> findall();
	public Item findById(Long id, Integer quantity);

}
