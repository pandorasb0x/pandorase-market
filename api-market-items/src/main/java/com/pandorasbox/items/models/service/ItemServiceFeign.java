package com.pandorasbox.items.models.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.pandorasbox.items.clients.ProductClientRest;
import com.pandorasbox.items.models.Item;

@Service
public class ItemServiceFeign implements ItemService {

	@Autowired
	private ProductClientRest feignClient;
	
	@Override
	public List<Item> findall() {
		return feignClient.products().stream().map(p -> new Item(p,1)).collect(Collectors.toList());
	}

	@Override
	public Item findById(Long id, Integer quantity) {
		return new Item(feignClient.getDetailedProd(id), quantity);
	}

}
