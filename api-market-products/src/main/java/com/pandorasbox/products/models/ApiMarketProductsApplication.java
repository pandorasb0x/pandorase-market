package com.pandorasbox.products.models;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMarketProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiMarketProductsApplication.class, args);
	}

}