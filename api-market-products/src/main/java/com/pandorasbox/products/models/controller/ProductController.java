package com.pandorasbox.products.models.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.pandorasbox.products.models.service.IProductService;
import com.pandorasbox.products.models.entity.Product;


@RestController
public class ProductController {
	
	@Autowired 	
	private Environment env;
	
	@Autowired
	private IProductService productService;
	
	@GetMapping("/products")
	public List<Product> getProducts(){
		return productService.findAll().stream().map(prod ->{
			prod.setPort(Integer.parseInt(env.getProperty("local.server.port")));
			
			return prod;
		}).collect(Collectors.toList());
			
	}
	
	@GetMapping("/products/{id}")
	public Product getDetailedProd(@PathVariable Long id){
		Product prod = productService.findById(id);
		prod.setPort(Integer.parseInt(env.getProperty("local.server.port")));
		return prod;
	}
}
