package com.pandorasbox.products.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.pandorasbox.products.models.entity.Product;

public interface ProductDao extends CrudRepository<Product, Long> {

}
