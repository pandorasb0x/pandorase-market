package com.pandorasbox.products.models.service;

import java.util.List;

import com.pandorasbox.products.models.entity.Product;

public interface IProductService {

	public List<Product> findAll();

	public Product findById(Long id);
}
