This is a personal project to put in practice different technologies to build scalable microservices.

Some of the chosen technologies are:

- Spring Boot 2
- Spring Cloud
- Eureka
- Zuul
- Ribbon
- Hystrix 
- API RESTful
- JPA
- H2
- OAuth
- Docker